#ifndef GEOMETRICA_H
#define GEOMETRICA_H

#include <iostream>

using namespace std;

class Geometrica{
    private:
        float base, altura, raio;
    public:


        float getBase();
	void setBase();
	
	float getAltura();
	void setAltura();
		
	float getRaio();
	void setRaio();
    


        float CalculaArea();
};

#endif
