#ifndef GEOMETRICA_H
#define GEOMETRICA_H

#include <iostream>

using namespace std;

class Geometrica{
    private:
        float base, altura,raio;
    public:
        Geometrica();
	~Geometrica

        float getBase();
	void setBase();
	
	float getAltura();
	void setAltura();
		
	float getRaio();
	void setRaio();
    


        virtual float area() = 0;
};

#endif
