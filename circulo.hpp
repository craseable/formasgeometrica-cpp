#ifndef CIRCULO_H
#define CIRCULO_H

#include "Geometrica.hpp"

using namespace std;

class circulo : public Geometrica{
public:
	circulo();
	~circulo();

	float getRaio();
	void setRaio();

	float CalculaArea(float raio);

};

#endif
