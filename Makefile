all:		geometrica.o retangulo.o circulo.o main.o
		g++ -o main geometrica.o retangulo.o circulo.o main.o

geometrica.o:	geometrica.cpp geometrica.hpp
		g++ -c geometrica.cpp

retangulo.o:	retangulo.cpp retangulo.hpp
		g++ -c retangulo.cpp

circulo.o: 	circulo.cpp circulo.hpp
		g++ -c circulo.cpp

main.o:		main.cpp retangulo.hpp
		g++ -c main.cpp

clean:
		rm -rf *.o

run:
		./main
