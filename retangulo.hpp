#ifndef RETANGULO_H
#define RETANGULO_H

#include "Geometrica.hpp"

class retangulo : public Geometrica{
    public:
        retangulo();
	~retangulo();

	float getBase();
	void setBase();

	float getAltura();
	void setAltura();

        float CalculaArea(float base, float altura);
};

#endif
